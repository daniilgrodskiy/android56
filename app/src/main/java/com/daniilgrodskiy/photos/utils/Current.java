package com.daniilgrodskiy.photos.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.model.Photo;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Current {
    public static Album CURRENT_ALBUM;
    public static Photo CURRENT_PHOTO;
    public static boolean IS_SEARCH_RESULT = false;

    public static List<Album> GET_ALL_ALBUMS(Context context) throws Exception {
        try {
            List<Album> albums = new ArrayList<>();

            System.out.println(context.getFilesDir().getAbsolutePath() + "/data.json");
            Object parsedObject = new JSONParser().parse(new FileReader(new File(context.getFilesDir(), "data.json")));

            System.out.println("All albums: " + parsedObject);

            JSONArray albumsJSON = (JSONArray) parsedObject;

            for (Object o : albumsJSON) {
                albums.add(Album.fromJSON((JSONObject) o));
            }

            return albums;
        } catch (Exception e) {
            System.out.println("Could not retrieve albums!");
            return new ArrayList<>();
        }
    }

    public static boolean UPDATE_ALBUM_NAME(Context context, Album oldAlbum, Album newAlbum) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);

        try {
            albums.removeIf(a -> a.getName().equals(oldAlbum.getName()));

            for (Album a : albums) {
                if (a.getName().equals(newAlbum.getName())) {
                    return false;
                }
            }

            albums.add(newAlbum);

            JSONArray jsonArray = new JSONArray();

            for (Album a : albums) {
                jsonArray.add(a.toJSON());
            }


            System.out.println(jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();

            CURRENT_ALBUM = newAlbum;
            return true;
        } catch (Exception e) {
            System.out.println("Album was not saved!");
            return false;
        }
    }

    public static boolean SAVE_ALBUM(Context context, Album album) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);

        try {
            albums.add(album);

            JSONArray jsonArray = new JSONArray();

            for (Album a : albums) {
                jsonArray.add(a.toJSON());
            }


            System.out.println(jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();

            return true;
        } catch (Exception e) {
            System.out.println("Album was not saved!");
            return false;
        }
    }

    public static boolean SAVE_PHOTO(Context context, Photo photo) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);

        System.out.println("THIS IS THE CURRENT ALBUM " + CURRENT_ALBUM.getName());

        try {

            JSONArray jsonArray = new JSONArray();

            for (Album a : albums) {
                if (a.getName().equals(CURRENT_ALBUM.getName())) {
                    // Album was found; add photo to this album
                    System.out.println("CURRENT ALBUM WAS FOUND!");
                    if (CURRENT_PHOTO !=  null) {
                        a.removePhoto(CURRENT_PHOTO.getPath());
                        CURRENT_PHOTO = null;
                    }
                    a.addPhoto(photo);
                    CURRENT_ALBUM = a;
                }
                jsonArray.add(a.toJSON());
            }

            System.out.println("New JSON file: " + jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();
            CURRENT_PHOTO = null;
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Album was not saved!");
            return false;
        }
    }

    public static boolean REMOVE_PHOTO(Context context) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);

        System.out.println("THIS IS THE CURRENT ALBUM " + CURRENT_ALBUM.getName());

        try {

            JSONArray jsonArray = new JSONArray();

            for (Album a : albums) {
                if (a.getName().equals(CURRENT_ALBUM.getName())) {
                    // Album was found; add photo to this album
                    System.out.println("CURRENT ALBUM WAS FOUND!");
                    a.removePhoto(CURRENT_PHOTO.getPath());
                    CURRENT_ALBUM = a;
                }
                jsonArray.add(a.toJSON());
            }



            System.out.println("New JSON file: " + jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();
            CURRENT_PHOTO = null;
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Photo was not deleted!");
            return false;
        }
    }

    public static boolean REMOVE_ALBUM(Context context) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);

        try {
            JSONArray jsonArray = new JSONArray();

            albums.removeIf(a -> a.getName().equals(CURRENT_ALBUM.getName()));

            for (Album a : albums) {
                CURRENT_ALBUM = null;
                jsonArray.add(a.toJSON());
            }

            System.out.println("New JSON file: " + jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();
            CURRENT_PHOTO = null;
            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Photo was not deleted!");
            return false;
        }
    }

    public static boolean MOVE_PHOTO(Context context, Photo photo, Album newAlbum) throws Exception {
        List<Album> albums = GET_ALL_ALBUMS(context);
        Album oldAlbum = newAlbum;

        try {
            JSONArray jsonArray = new JSONArray();

            for (Album a : albums) {
                for (Photo p : a.getPhotos()) {
                    if (p.getPath().equals(photo.getPath())) {
                        oldAlbum = a;
                        break;
                    }
                }
            }


            for (Album a : albums) {
                a.getPhotos().removeIf(p -> p.getPath().equals(photo.getPath()));
            }

            for (Album a : albums) {
                if (a.getName().equals(newAlbum.getName())) {
                    a.addPhoto(photo);
                }
                jsonArray.add(a.toJSON());
            }

            CURRENT_ALBUM = oldAlbum;
            System.out.println("New JSON file: " + jsonArray);

            FileWriter out = new FileWriter(new File(context.getFilesDir(), "data.json"));
            out.write(jsonArray.toJSONString());
            out.close();

            return true;
        } catch (Exception e) {
            System.out.println(e.toString());
            System.out.println("Photo was not deleted!");
            return false;
        }
    }
}
