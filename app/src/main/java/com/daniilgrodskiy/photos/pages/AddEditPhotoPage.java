package com.daniilgrodskiy.photos.pages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.dialogs.AddEditAlbumDialog;
import com.daniilgrodskiy.photos.dialogs.MovePhotoDialog;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.recycler_views.AlbumsRecyclerViewAdapter;
import com.daniilgrodskiy.photos.utils.Current;

import java.util.ArrayList;
import java.util.List;

public class AddEditPhotoPage extends AppCompatActivity implements MovePhotoDialog.MovePhotoDialogListener {

    private TextView addEditPhotoPageTitle;
    private Button selectPhotoButton;
    private ImageView photoImage;
    private EditText locationTagField;
    private EditText personTagField;
    private Button photoSubmitButton;
    private Button removePhotoButton;
    private Button movePhotoButton;

    private String photoPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_photo_page);
        initState();
    }

    private void initState() {
        addEditPhotoPageTitle = findViewById(R.id.addEditPhotoPageTitle);;
        selectPhotoButton = findViewById(R.id.selectPhotoButton);
        photoImage = findViewById(R.id.currentPhotoImage);
        locationTagField = findViewById(R.id.locationTagField);
        personTagField = findViewById(R.id.personTagField);
        photoSubmitButton = findViewById(R.id.photoSubmitButton);
        removePhotoButton = findViewById(R.id.removePhotoButton);
        movePhotoButton = findViewById(R.id.movePhotoButton);


        if (Current.CURRENT_PHOTO != null) {
            addEditPhotoPageTitle.setText("Edit Photo");
            selectPhotoButton.setVisibility(View.GONE);
            photoPath = Current.CURRENT_PHOTO.getPath();
            // Update photo image
            try {
                final int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                this.getContentResolver().takePersistableUriPermission(Uri.parse(Current.CURRENT_PHOTO.getPath()), takeFlags);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(Current.CURRENT_PHOTO.getPath()));
                photoImage.setImageBitmap(bitmap);
            } catch(Exception e) {
                System.out.println(e.toString());
            }

            locationTagField.setText(Current.CURRENT_PHOTO.getLocationTag());
            personTagField.setText(Current.CURRENT_PHOTO.getPersonTag());
            photoSubmitButton.setText("Save");
        } else {
            movePhotoButton.setVisibility(View.GONE);
            addEditPhotoPageTitle.setText("Add Photo");
            photoSubmitButton.setText("Create");
            removePhotoButton.setVisibility(View.GONE);
        }
        // TODO: Add the code to fill the fields with data if Current.PHOTO is not null
    }

    @Override
    public void finish() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
        super.finish();
    }

    public void handleRemovePhotoButton(View view) throws Exception {
        Current.REMOVE_PHOTO(this);
        finish();
    }

    public void handleSubmitButton(View view) throws Exception {
        // TODO: Run code to add photo to the photos array in the Current.ALBUM in JSON
        if (photoPath == null || photoPath.equals("")) {
            Toast.makeText(this, "You must select a photo", Toast.LENGTH_SHORT).show();
            return;
        }

        // Make sure this photo is nowhere else in the app
        if (Current.CURRENT_PHOTO == null) {
            for (Album a : Current.GET_ALL_ALBUMS(this)) {
                if (a.getPhotos() != null) {
                    for (Photo p : a.getPhotos()) {
                        if (p.getPath().equals(photoPath)) {
                            Toast.makeText(this, "This photo has already been added", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }
            }
        }

        // TODO: There was a problem with the path saving and being good?
        try {
            Photo newPhoto = new Photo(photoPath, personTagField.getText().toString(), locationTagField.getText().toString());
            Current.SAVE_PHOTO(this, newPhoto);
        } catch (Exception e) {
            Toast.makeText(this, "Error saving photo", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    public void handleSelectPhotoButton(View view) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");

        startActivityForResult(Intent.createChooser(intent, "Select Photo"), 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 200) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    photoPath = selectedImageUri.toString();
                    photoImage.setImageURI(selectedImageUri);
                }
            }
        }
    }

    public void handleMovePhotoButton(View view) {
        // Show dialog to ask where to move photo
        MovePhotoDialog addEditAlbumDialog = new MovePhotoDialog();
        addEditAlbumDialog.show(getSupportFragmentManager(), "Move Photo Dialog");
    }

    public void dialogCallback(Album newAlbum) {
        try {
            if (Current.MOVE_PHOTO(this, Current.CURRENT_PHOTO, newAlbum)) {
                Toast.makeText(this, "Photo was successfully moved to " + newAlbum.getName(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}