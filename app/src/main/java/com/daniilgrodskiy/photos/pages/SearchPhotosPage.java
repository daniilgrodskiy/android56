package com.daniilgrodskiy.photos.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.utils.Current;

import java.util.ArrayList;
import java.util.List;

public class SearchPhotosPage extends AppCompatActivity {

    private EditText searchPersonTagField;
    private EditText searchLocationTagField;
    private Button photoSubmitButton;

    @Override
    protected void onResume() {
        // Runs when we go back to this page
        super.onResume();
        Current.IS_SEARCH_RESULT = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_photos_page);
        initState();
    }

    public void initState() {
        searchPersonTagField = findViewById(R.id.searchPersonTagField);
        searchLocationTagField = findViewById(R.id.searchLocationTagField);
        photoSubmitButton = findViewById(R.id.photoSubmitButton);
    }

    public void handleSearchButton(View view) {

        String personQuery = searchPersonTagField.getText().toString().toLowerCase();
        String locationQuery = searchLocationTagField.getText().toString().toLowerCase();

        if (personQuery.equals("") && locationQuery.equals("")) {
            // Fields are both empty
            Toast.makeText(this, "Person and tag fields cannot both be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        // Navigate to new album page
        try {
            List<Photo> photosToAdd = new ArrayList<>();
            List<Album> albums = Current.GET_ALL_ALBUMS(this);

            // Go through all the photos and find all photos that fit the tag criteria
            for (Album a : albums) {
                for (Photo p : a.getPhotos()) {
                    if (!personQuery.isEmpty() && p.getPersonTag().toLowerCase().startsWith(personQuery)) {
                        if (!locationQuery.isEmpty()) {
                            if (p.getLocationTag().toLowerCase().startsWith(locationQuery)) {
                                photosToAdd.add(p);
                            }
                        } else {
                            photosToAdd.add(p);
                        }

                    } else {
                        if (!locationQuery.isEmpty() && p.getLocationTag().toLowerCase().startsWith(locationQuery)) {
                            if (!personQuery.isEmpty()) {
                                if (p.getPersonTag().toLowerCase().startsWith(personQuery)) {
                                    photosToAdd.add(p);
                                }
                            } else {
                                photosToAdd.add(p);
                            }
                        }
                    }
                }
            }

            if (photosToAdd.size() == 0) {
                Toast.makeText(this, "No photos found with this query", Toast.LENGTH_SHORT).show();
                return;
            }

            Current.IS_SEARCH_RESULT = true;
            Current.CURRENT_ALBUM = new Album("Search Results", photosToAdd);;
            Intent intent = new Intent(this, PhotosPage.class);
            startActivity(intent);

        } catch (Exception e) {
            System.out.println(e.toString());
        }



    }
}