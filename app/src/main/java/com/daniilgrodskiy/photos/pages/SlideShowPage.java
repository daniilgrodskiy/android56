package com.daniilgrodskiy.photos.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.utils.Current;

import java.util.List;

public class SlideShowPage extends AppCompatActivity {

    private ImageView slideShowImageView;
    private Button previousPhotoButton;
    private Button nextPhotoButton;

    private int currentIndex = 0;

    private List<Photo> photos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_show_page);

        initData();
    }

    public void initData() {
        slideShowImageView = findViewById(R.id.slideShowImageView);
        previousPhotoButton = findViewById(R.id.previousPhotoButton);
        nextPhotoButton = findViewById(R.id.nextPhotoButton);

        photos = Current.CURRENT_ALBUM.getPhotos();

        showNewImage();
        changeVisibilityOfButtons();
    }

    public void handlePreviousPhotoButton(View view) {
        currentIndex--;
        showNewImage();
        changeVisibilityOfButtons();
    }

    public void handleNextPhotoButton(View view) {
        currentIndex++;
        showNewImage();
        changeVisibilityOfButtons();
    }

    public void showNewImage() {
        System.out.println("PHOTOS: " + photos.toString());
        System.out.println("CURRENT INDEX: " + currentIndex);
        try {
            final int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            this.getContentResolver().takePersistableUriPermission(Uri.parse(photos.get(currentIndex).getPath()), takeFlags);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(photos.get(currentIndex).getPath()));
            slideShowImageView.setImageBitmap(bitmap);
        } catch(Exception e) {
            System.out.println(e.toString());
        }
    }

    public void changeVisibilityOfButtons() {
        previousPhotoButton.setVisibility(View.GONE);
        nextPhotoButton.setVisibility(View.GONE);

        if (photos.size() == 1) {
            // No buttons to show
            return;
        }

        if (currentIndex > 0) {
            previousPhotoButton.setVisibility(View.VISIBLE);
        }

        if (currentIndex < photos.size() - 1) {
            nextPhotoButton.setVisibility(View.VISIBLE);

        }
    }
}