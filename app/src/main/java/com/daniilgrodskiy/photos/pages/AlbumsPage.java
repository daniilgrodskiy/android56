package com.daniilgrodskiy.photos.pages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.dialogs.AddEditAlbumDialog;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.recycler_views.AlbumsRecyclerViewAdapter;
import com.daniilgrodskiy.photos.utils.Current;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class AlbumsPage extends AppCompatActivity implements AddEditAlbumDialog.AddEditAlbumDialogListener {

    private TextView editAlbumField;
    private TextView photosHeadingText;
    private Button addAlbumButton;
    private ImageView photoImage;
    private RecyclerView albumsRecyclerView;
    private AlbumsRecyclerViewAdapter albumsAdapter;

    @Override
    protected void onResume() {
        // Runs when we go back to this page
        super.onResume();
        Current.CURRENT_ALBUM = null;
        if (albumsAdapter != null) {
            try {
                albumsAdapter.setAlbums(Current.GET_ALL_ALBUMS(this));
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums_page);
        initState();
    }

    private void initState() {
        albumsRecyclerView = findViewById(R.id.albumsRecyclerView);
        addAlbumButton = findViewById(R.id.addAlbumButton);

        // Create the adapter
        try {
            albumsAdapter= new AlbumsRecyclerViewAdapter(this, Current.GET_ALL_ALBUMS(this));
            albumsRecyclerView.setAdapter(albumsAdapter);
            albumsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        } catch (Exception e) {
            System.out.println("There was an error connecting the adapter!");
        }
    }

    public void handleAddAlbumButton(View view) {
        AddEditAlbumDialog addEditAlbumDialog = new AddEditAlbumDialog();
        addEditAlbumDialog.show(getSupportFragmentManager(), "Add Album Dialog");
    }

    public void handleSearchPhotosButton(View view) {
        Intent intent = new Intent(this, SearchPhotosPage.class);
        startActivity(intent);
    }

    @Override
    public void applyTexts(String albumName) {
        if (albumName.equals("")) {
            Toast.makeText(this, "Album name cannot be null", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Album album = new Album(albumName, new ArrayList<>());
            if (!Current.SAVE_ALBUM(this, album)) {
                Toast.makeText(this, "Error saving album!", Toast.LENGTH_SHORT).show();
                return;
            }

        } catch(Exception e) {
            System.out.println(e);
        }


        Toast.makeText(this, albumName +  " successfully added!", Toast.LENGTH_SHORT).show();
        AlbumsRecyclerViewAdapter adapter = (AlbumsRecyclerViewAdapter) albumsRecyclerView.getAdapter();

        assert adapter != null;
        adapter.addAlbum(new Album(albumName, new ArrayList<>()));
    }
}