package com.daniilgrodskiy.photos.pages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.dialogs.AddEditAlbumDialog;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.recycler_views.AlbumsRecyclerViewAdapter;
import com.daniilgrodskiy.photos.recycler_views.PhotosRecyclerViewAdapter;
import com.daniilgrodskiy.photos.utils.Current;

import java.util.ArrayList;
import java.util.List;

public class PhotosPage extends AppCompatActivity implements AddEditAlbumDialog.AddEditAlbumDialogListener {
    private TextView photosTitle;
    private Button addPhotoButton;
    private RecyclerView photosRecyclerView;
    private PhotosRecyclerViewAdapter photosAdapter;
    private LinearLayout albumButtons;

    @Override
    protected void onResume() {
        // Runs when we go back to this page
        super.onResume();
        Current.CURRENT_PHOTO = null;
        if(photosAdapter != null) {
            // Reset adapter
            System.out.println("Back to PhotosPage");
            photosAdapter = new PhotosRecyclerViewAdapter(this, Current.CURRENT_ALBUM.getPhotos() == null ? new ArrayList<>() : Current.CURRENT_ALBUM.getPhotos());
            photosRecyclerView.setAdapter(photosAdapter);
            photosRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }

        if (photosTitle != null) {
            photosTitle.setText(Current.CURRENT_ALBUM.getName());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_page);
        initState();
    }

    private void initState() {
        photosRecyclerView = findViewById(R.id.photosRecyclerView);
        addPhotoButton = findViewById(R.id.addPhotoButton);
        photosTitle = findViewById(R.id.photosTitle);
        albumButtons = findViewById(R.id.albumButtons);

        if (Current.IS_SEARCH_RESULT) {
            photosTitle.setText(Current.CURRENT_ALBUM.getName());
            albumButtons.setVisibility(View.GONE);
            addPhotoButton.setVisibility(View.GONE);
        } else {
            photosTitle.setText(Current.CURRENT_ALBUM.getName());
        }

        // Create the adapter
        try {
            photosAdapter = new PhotosRecyclerViewAdapter(this, Current.CURRENT_ALBUM.getPhotos() == null ? new ArrayList<>() : Current.CURRENT_ALBUM.getPhotos());
            photosRecyclerView.setAdapter(photosAdapter);
            photosRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        } catch (Exception e) {
            System.out.println("There was an error connecting the adapter!");
        }


    }

    public void handleAddPhotoButton(View view) {
        Intent intent = new Intent(this, AddEditPhotoPage.class);
        startActivity(intent);
    }

    public void handleRenamePhotoButton(View view) {
        AddEditAlbumDialog addEditAlbumDialog = new AddEditAlbumDialog();
        addEditAlbumDialog.show(getSupportFragmentManager(), "Add Album Dialog");
    }

    @Override
    public void applyTexts(String albumName) {
        if (albumName.equals("")) {
            Toast.makeText(this, "New name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Album newAlbum = new Album(albumName, Current.CURRENT_ALBUM.getPhotos());
            if (!Current.UPDATE_ALBUM_NAME(this, Current.CURRENT_ALBUM, newAlbum)) {
                Toast.makeText(this, "An album with this name already exists!", Toast.LENGTH_SHORT).show();
                return;
            }

        } catch(Exception e) {
            System.out.println(e);
        }

        photosTitle.setText(Current.CURRENT_ALBUM.getName());
    }

    public void handleSlideShowButton(View view) {
        if (Current.CURRENT_ALBUM.getPhotos().size() == 0) {
            // No photos to show
            Toast.makeText(this, "No photos to show in slide show!", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, SlideShowPage.class);
        startActivity(intent);
    }

    public void handleRemoveAlbumButton(View view) {
        try {
            Current.REMOVE_ALBUM(this);
        } catch(Exception e) {
            System.out.println(e.toString());
        }
        finish();
    }

}