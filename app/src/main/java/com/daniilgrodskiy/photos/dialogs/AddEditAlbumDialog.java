package com.daniilgrodskiy.photos.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.utils.Current;

public class AddEditAlbumDialog extends AppCompatDialogFragment {
    private EditText albumNameField;
    private AddEditAlbumDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.add_edit_album_dialog, null);

        builder.setView(view)
                .setTitle(Current.CURRENT_ALBUM == null ? "Add Album" : "Edit Album")
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                })
                .setPositiveButton("Save", (dialogInterface, i) -> {
                    String albumName = albumNameField.getText().toString();
                    listener.applyTexts(albumName);
                });

        albumNameField = view.findViewById(R.id.albumNameField);

        if (Current.CURRENT_ALBUM != null) {
            albumNameField.setText(Current.CURRENT_ALBUM.getName());
        }

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (AddEditAlbumDialogListener) context;
        } catch(ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddEditAlbumDialogListener");
        }
    }

    public interface AddEditAlbumDialogListener {
        void applyTexts(String albumName);
    }
}
