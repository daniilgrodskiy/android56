package com.daniilgrodskiy.photos.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.recycler_views.AlbumsRecyclerViewAdapter;
import com.daniilgrodskiy.photos.utils.Current;

public class MovePhotoDialog extends AppCompatDialogFragment {
    private RecyclerView albumsRecyclerView;
    private AlbumsRecyclerViewAdapter albumsAdapter;
    private MovePhotoDialogListener listener;
    private TextView selectedAlbumTextView;

    public TextView getSelectedAlbumTextView() {
        return selectedAlbumTextView;
    }

    public void setSelectedAlbumTextViewText(String text) {
        selectedAlbumTextView.setText(text);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.move_photo_dialog, null);

        albumsRecyclerView = view.findViewById(R.id.albumsRecyclerView);
        selectedAlbumTextView = view.findViewById(R.id.selectedAlbumTextView);

        // Create the adapter
        try {
            albumsAdapter= new AlbumsRecyclerViewAdapter(view.getContext(), Current.GET_ALL_ALBUMS(view.getContext()), this);
            albumsRecyclerView.setAdapter(albumsAdapter);
            albumsRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        } catch (Exception e) {
            System.out.println("There was an error connecting the adapter!");
        }

        builder.setView(view)
                .setTitle("Choose New Album")
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                })
                .setPositiveButton("Move", (dialogInterface, i) -> {
                    System.out.println("ALBUM INDEX" + albumsAdapter.getSelectedIndex());
                    if (albumsAdapter.getSelectedIndex() == -1) {
                        Toast.makeText(view.getContext(), "No album was selected!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    listener.dialogCallback(albumsAdapter.getAlbums().get(albumsAdapter.getSelectedIndex()));
                });

        return builder.create();
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (MovePhotoDialogListener) context;
        } catch(ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddEditAlbumDialogListener");
        }
    }

    public interface MovePhotoDialogListener {
        void dialogCallback(Album newAlbum);
    }
}
