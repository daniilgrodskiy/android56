package com.daniilgrodskiy.photos.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Photo {
    private String path;
    private String personTag;
    private String locationTag;

    public Photo(String path, String personTag, String locationTag) {
        this.path = path;
        this.personTag = personTag;
        this.locationTag = locationTag;
    }

    /** JSON METHODS **/

    public JSONObject toJSON() {
        JSONObject photoJSON = new JSONObject();
        photoJSON.put("path", this.getPath());
        photoJSON.put("personTag", this.getPersonTag().trim());
        photoJSON.put("locationTag", this.getLocationTag().trim());
        return photoJSON;
    }

    public static Photo fromJSON(JSONObject photoJSON) {
        return new Photo(
                (String) photoJSON.get("path"),
                (String) photoJSON.get("personTag"),
                (String) photoJSON.get("locationTag")
        );

    }

    /** GETTERS AND SETTERS **/

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPersonTag() {
        return personTag;
    }

    public void setPersonTag(String personTag) {
        this.personTag = personTag;
    }

    public String getLocationTag() {
        return locationTag;
    }

    public void setLocationTag(String locationTag) {
        this.locationTag = locationTag;
    }
}
