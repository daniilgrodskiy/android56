package com.daniilgrodskiy.photos.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Album {
    private String name;
    private List<Photo> photos = new ArrayList<>();

    public Album(String name, List<Photo> photos) {
        this.name = name;
        this.photos = photos;
    }

    /** JSON METHODS **/

    public JSONObject toJSON() {
        JSONObject albumJSON = new JSONObject();
        albumJSON.put("name", this.getName().trim());

        JSONArray photosJSON = new JSONArray();

        for (Photo p : photos) {
            photosJSON.add(p.toJSON());
            System.out.println("\n\nADDED PHOTO!!!!\n\n");
        }

        albumJSON.put("photos", photosJSON);
        return albumJSON;
    }

    public static Album fromJSON(JSONObject albumJSON) {
        String name = (String) albumJSON.get("name");
        List<Photo> photos = new ArrayList<>();

        JSONArray photosJSON = (JSONArray) albumJSON.get("photos");

        if (photosJSON != null) {
            for (Object o : photosJSON) {
                photos.add(Photo.fromJSON((JSONObject) o));
            }
        }

        return new Album(
                name,
                photos);
    }

    /** GETTERS AND SETTERS **/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
        System.out.println("NEW PHOTO WAS ADDED!");
    }

    public void removePhoto(String photoToRemovePath) {
        this.photos.removeIf(photo -> photo.getPath().equals(photoToRemovePath));
    }
}
