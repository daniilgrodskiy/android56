package com.daniilgrodskiy.photos.recycler_views;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.dialogs.MovePhotoDialog;
import com.daniilgrodskiy.photos.model.Album;
import com.daniilgrodskiy.photos.pages.AlbumsPage;
import com.daniilgrodskiy.photos.pages.PhotosPage;
import com.daniilgrodskiy.photos.utils.Current;

import java.util.ArrayList;
import java.util.List;

public class AlbumsRecyclerViewAdapter extends RecyclerView.Adapter<AlbumsRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Album> albums = new ArrayList<>();
    private MovePhotoDialog movePhotoDialog = null;
    private int selectedIndex = -1;

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public AlbumsRecyclerViewAdapter(Context context, List<Album> albums) {
        this.context = context;
        this.albums = albums;
    }

    public AlbumsRecyclerViewAdapter(Context context, List<Album> albums, MovePhotoDialog movePhotoDialog) {
        this.context = context;
        this.albums = albums;
        this.movePhotoDialog = movePhotoDialog;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.albumName.setText(albums.get(position).getName());

        holder.parent.setOnClickListener(view -> {
            selectedIndex = position;
            if (movePhotoDialog != null) {
//                Toast.makeText(context, "Selected: " + albums.get(position).getName(), Toast.LENGTH_SHORT).show();
                if (movePhotoDialog.getSelectedAlbumTextView() != null) {
                    movePhotoDialog.setSelectedAlbumTextViewText(albums.get(position).getName());
                }
            } else {
                Current.CURRENT_ALBUM = albums.get(position);
                Intent intent = new Intent(context, PhotosPage.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.albums.size();
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void addAlbum(Album album) {
        this.albums.add(album);
        notifyDataSetChanged();

    }

    public void removeAlbum(Album album) {
        this.albums.remove(album);
        notifyDataSetChanged();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView parent;
        private TextView albumName;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parent = itemView.findViewById(R.id.parent);
            albumName = itemView.findViewById(R.id.albumName);
        }
    }


}

