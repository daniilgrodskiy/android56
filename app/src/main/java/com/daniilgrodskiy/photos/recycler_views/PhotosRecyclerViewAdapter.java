package com.daniilgrodskiy.photos.recycler_views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daniilgrodskiy.photos.R;
import com.daniilgrodskiy.photos.model.Photo;
import com.daniilgrodskiy.photos.pages.AddEditPhotoPage;
import com.daniilgrodskiy.photos.utils.Current;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PhotosRecyclerViewAdapter extends RecyclerView.Adapter<PhotosRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Photo> photos = new ArrayList<>();

    public PhotosRecyclerViewAdapter(Context context, List<Photo> photos) {
        this.context = context;
        this.photos = photos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // This code runs on each photo (when it gets bound to widget tree)

        try {
            // Find image and update PhotoView
            final int takeFlags =  (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            context.getContentResolver().takePersistableUriPermission(Uri.parse(photos.get(position).getPath()), takeFlags);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.parse(photos.get(position).getPath()));
            holder.photoImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            System.out.println("PHOTO WAS NOT FOUND!");
            System.out.println(e.toString());
        }

        if (photos.get(position).getPersonTag().equals("")) {
            // No person tag
            holder.personParent.setVisibility(View.GONE);
        } else {
            holder.personTag.setText(photos.get(position).getPersonTag());
        }

        if (photos.get(position).getLocationTag().equals("")) {
            // No location tag
            holder.locationParent.setVisibility(View.GONE);
        } else {
            holder.locationTag.setText(photos.get(position).getLocationTag());
        }


        if (!Current.IS_SEARCH_RESULT) {
            // Only allow clicking if it's NOT a search result
            holder.parent.setOnClickListener(view -> {
                Intent intent = new Intent(context, AddEditPhotoPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Current.CURRENT_PHOTO = photos.get(position);
                context.getApplicationContext().startActivity(intent);
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.photos.size();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView parent;
        private RelativeLayout personParent, locationParent;
        private TextView personTag, locationTag;
        private ImageView photoImage;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);

            photoImage = itemView.findViewById(R.id.photoImage);
            personTag = itemView.findViewById(R.id.personTag);
            locationTag = itemView.findViewById(R.id.locationTag);

            personParent = itemView.findViewById(R.id.personParent);
            locationParent = itemView.findViewById(R.id.locationParent);
        }
    }


}
